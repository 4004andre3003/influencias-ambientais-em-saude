
# <center> MAC0434/6967 – Grupo Influências Ambientais em Saúde

## <center> Andre Marques, Denise Florio, Felipe Noronha, Izabela Fonseca, Nicolas Vana

---

## Questões de pesquisa

- É possível inferir causalidades em internações hospitalares a partir dos dados demográficos e de entorno do CENSO do IBGE?
- Quais doenças são decorrentes de interferências ambientais e urbanas na saúde?
- Quais são os fatores ambientais (tudo o que afeta o local que a pessoa mora, interno e externamente) que influenciam na causalidade das doenças?


## Introdução

O projeto desta equipe visa correlacionar aspectos ambientais com algumas doenças para gerarmos insights e se possível um modelo, visando facilitar e embasar o planejamento de ações e investimentos de órgãos governamentais.

Após entendimento inicial do objetivo do projeto, obtido em reunião realizada com o Instituto Cordial no dia 02 de Setembro de 2020, foi definido o foco inicial do projeto, que nesse momento será em doenças do trato digestivo e doenças respiratórias, além do foco no estado de São Paulo.

Para viabilizar as análises desejadas, foram disponibilizados 19 arquivos contendo informações do IBGE (Censo 2010) de Domicílios e Entorno, trazendo informações sobre os municípios e seus moradores, e a base do Sistema de Informações Hospitalares (SIH) (Datasensus 2017) com informações de internações na rede pública do estado de São Paulo de diversos pacientes e suas respectivas enfermidades devidamente classificadas de acordo com a base de Classificação Internacional de Doenças (CID10).

## Estrutura do repositório

```bash 

├── analises-exploratorias -> notebooks utilizados nas analises exploratórias
├── cronograma.xlsx -> cronograma de entregas e desenvolvimento
├── dados -> planilhas e dicionários com os datasets usados
├── entregas -> documentos das entregas de cada fase
├── Estatistica_básica_do_set__sem_São_Paulo_.ipynb
├── misc -> arquivos sem categoria definida
├── README.md
└── referencias -> documentos e links com pesquisas uteis para referencias

```
